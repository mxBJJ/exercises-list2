package exercises.list2.classes;

public class ContaCorrente {
	
	private double saldo;
	private int nConta;
	
	
	public ContaCorrente(double saldo, int nConta) {
		super();
		this.saldo = saldo;
		this.nConta = nConta;
	}

	public void deposito(double valor) {
		this.saldo = saldo + valor;
		System.out.println("Valor do depósito: R$ " + valor);
		System.out.println("Saldo atual: R$ " + saldo);

	}
	
	public double retirada(double valor) {
		this.saldo = saldo - valor;
		return saldo;
	}
	
	public double getSaldo() {
		return this.saldo;
	}
	
	public int getNroConta() {
		return this.nConta;
	}

	@Override
	public String toString() {
		return "ContaCorrente [saldo=" + saldo + ", nConta=" + nConta + "]";
	}
	
}
