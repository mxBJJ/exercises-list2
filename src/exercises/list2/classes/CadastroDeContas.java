package exercises.list2.classes;

import java.util.HashMap;
import java.util.Map;

public class CadastroDeContas {
	
	private Map<Integer, Double> contasCadastradas = new HashMap<>();
	
	public CadastroDeContas() {
		super();
	}


	public boolean novaContaCorrente(ContaCorrente c) {
		
		if(c != null) {
			contasCadastradas.put(c.getNroConta(), c.getSaldo());
			return true;
		}
		
		return false;
	}
	
	
	public ContaCorrente getConta(int nroCta) {
		
		if(contasCadastradas.get(nroCta) == null || contasCadastradas.containsKey(nroCta)) {
			return null;
		}
		
		
		double saldo = contasCadastradas.get(nroCta);
		
		System.out.println(saldo);
		
		ContaCorrente conta = new ContaCorrente(saldo, nroCta);
		return conta;
	}
	
	public double getSaldoContas() {
		
		double total = 0;
		
		for(double saldo : contasCadastradas.values()) {
			total = total + saldo;
		}
				
		return total;
	}
	
	
	public int qtdContas(double noMinimo) {
		
		int cont = 0;
		
		for(double saldo : contasCadastradas.values()) {
			if(noMinimo >= noMinimo) {
				cont = cont + 1;
			}
		}
				
		return cont;
	}
}
