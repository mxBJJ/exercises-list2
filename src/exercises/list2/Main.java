package exercises.list2;

import exercises.list2.classes.CadastroDeContas;
import exercises.list2.classes.ContaCorrente;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ContaCorrente conta1 = new ContaCorrente(2000.00,  12345);
		CadastroDeContas cadastro = new CadastroDeContas();
		
		
		ContaCorrente conta2 = new ContaCorrente(2000.00,  123456);
		
		
		cadastro.novaContaCorrente(conta1);
		cadastro.novaContaCorrente(conta2);

		System.out.println(cadastro.getSaldoContas());
	}
}
